#!/bin/bash

docker rm -f etcd vulcand1 vulcand2

export HostIP="localhost"

docker run -d -p 4001:4001 -p 2380:2380 -p 2379:2379 \
 --name etcd quay.io/coreos/etcd:v2.3.2 \
 -name etcd0 \
 -advertise-client-urls http://${HostIP}:2379,http://${HostIP}:4001 \
 -listen-client-urls http://0.0.0.0:2379,http://0.0.0.0:4001 \
 -initial-advertise-peer-urls http://${HostIP}:2380 \
 -listen-peer-urls http://0.0.0.0:2380 \
 -initial-cluster-token etcd-cluster-1 \
 -initial-cluster etcd0=http://${HostIP}:2380 \
 -initial-cluster-state new

export EtcdIP=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' etcd)

export Httpd1IP=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' httpd1)

export Httpd2IP=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' httpd2)

export Httpd3IP=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' httpd3)

export Httpd4IP=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' httpd4)

# Upsert backend and add a server to it
# See for more settings: http://vulcand.github.io/proxy.html#backends-and-servers
curl -X PUT http://${HostIP}:4001/v2/keys/vulcand/backends/b1/backend \
        -d value='{"Type": "http"}'

curl -X PUT http://${HostIP}:4001/v2/keys/vulcand/backends/b1/servers/httpd1 \
        -d value="{\"URL\": \"http://${Httpd1IP}:80\"}"

curl -X PUT http://${HostIP}:4001/v2/keys/vulcand/backends/b1/servers/httpd2 \
        -d value="{\"URL\": \"http://${Httpd2IP}:80\"}"

curl -X PUT http://${HostIP}:4001/v2/keys/vulcand/backends/b1/servers/httpd1 \
        -d value="{\"URL\": \"http://${Httpd3IP}:80\"}"

curl -X PUT http://${HostIP}:4001/v2/keys/vulcand/backends/b1/servers/httpd1 \
        -d value="{\"URL\": \"http://${Httpd4IP}:80\"}"

# Upsert a frontend connected to backend "b1" that matches path /
# See for more settings: http://vulcand.github.io/proxy.html#frontends
curl -X PUT http://${HostIP}:4001/v2/keys/vulcand/frontends/f1/frontend \
        -d value='{"Type": "http", "BackendId": "b1", "Route": "Path(`/`)"}'

# download vulcand from the trusted build
#docker pull mailgun/vulcand:v0.8.0-beta.2

# launch vulcand in a container
docker run -d --name vulcand1 -p 8182:8182 -p 8181:8181 mailgun/vulcand:v0.8.0-beta.2 /go/bin/vulcand -apiInterface=0.0.0.0 --etcd=http://${EtcdIP}:4001

export Vulcand1IP=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' vulcand1)

curl -v http://${Vulcand1IP}:8181

docker run -d --name vulcand2 -p 8184:8182 -p 8183:8181 mailgun/vulcand:v0.8.0-beta.2 /go/bin/vulcand -apiInterface=0.0.0.0 --etcd=http://${EtcdIP}:4001

export Vulcand2IP=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' vulcand2)

curl -v http://${Vulcand2IP}:8181
