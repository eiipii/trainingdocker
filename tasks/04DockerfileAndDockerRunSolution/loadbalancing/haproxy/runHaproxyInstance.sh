#!/bin/bash

docker rm -f haproxy1

cd instance1

export Vulcand1IP=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' vulcand1)

docker build --build-arg VULCAND1ARG=${Vulcand1IP} -t my-haproxy1 .

docker run -d -p 8082:8080 --name haproxy1 my-haproxy1
