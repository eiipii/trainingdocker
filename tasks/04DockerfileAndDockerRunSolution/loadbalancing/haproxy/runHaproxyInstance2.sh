#!/bin/bash

docker rm -f haproxy2

cd instance2

export Vulcand2IP=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' vulcand2)

docker build -t my-haproxy2 .

docker run -d -e "VULCAND2=${Vulcand2IP}" -p 8083:8080 --name haproxy2 my-haproxy2
