#!/bin/bash

docker rm -f httpd1 httpd2 httpd3 httpd4

docker build -t my-apache2 .

docker run -d -P --name httpd1 my-apache2

docker run -d -P --name httpd2 my-apache2

docker run -d -P --name httpd3 my-apache2

docker run -d -P --name httpd4 my-apache2